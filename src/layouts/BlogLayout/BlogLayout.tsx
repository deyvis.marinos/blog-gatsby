import { Menu } from '@/components/organisms/Menu';
import { BlogLayoutProps } from '@/interfaces';
import { Container, Grid } from 'semantic-ui-react';
import './blogLayout.scss';

export const BlogLayout = ({ children, className }: BlogLayoutProps) => {
  return (
    <Container className={`l-blogLayout ${className}`} fluid>
      <Grid>
        <Grid.Column mobile={16} tablet={16} computer={4}>
          <Menu />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={12}>
          {children}
        </Grid.Column>
      </Grid>
    </Container>
  );
};
