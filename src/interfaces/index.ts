import React from 'react';
import { CoursesData, SocialMediaData } from '../types/local';

export interface BlogLayoutProps {
  className?: string;
  children: React.ReactNode;
}

export interface SeoProps {
  children?: React.ReactNode;
  description?: string;
  image?: string;
  title: string;
}

export interface SocialMediaProps {
  listSocial: SocialMediaData[];
}

export interface CoursesProps {
  listCourses: CoursesData[];
}

export interface BodyResponse {
  id: string;
  title: string;
  url: string;
  content: string;
  createdAt: string;
  miniature: [
    {
      url: string;
    },
  ];
  seo_title: string;
  seo_description: string;
}

export interface PostProps {
  pageContext: {
    data: BodyResponse;
  };
}

export interface PageContextProps {
  page: number;
  pages: number;
  limit: number;
  nextPagePath: string;
  numberOfPages: number;
  pageNumber: number;
  previousPagePath: string;
}

export interface PaginationProps {
  pageContext: PageContextProps;
}

export interface PostListProps {
  posts: BodyResponse[];
}

export interface BlogPosts {
  data: {
    allStrapiPost: {
      nodes: BodyResponse[];
    };
  };
  pageContext: PageContextProps;
}
