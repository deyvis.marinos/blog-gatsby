import { Seo } from '@/components/Seo';
import { PostProps } from '@/interfaces';
import { BlogLayout } from '@/layouts/BlogLayout';
import TransformOembedToIframe from '@/utils/transformOembed';
import './post.scss';

const Post = ({ pageContext }: PostProps) => {
  const { data } = pageContext;

  return (
    <BlogLayout className="p-post">
      <h1 className="p-post__title">{data?.title}</h1>
      <main className="p-post__markdown">
        <div
          dangerouslySetInnerHTML={{
            __html: TransformOembedToIframe(data?.content),
          }}
        />
      </main>
    </BlogLayout>
  );
};

export const Head = ({ pageContext }: PostProps) => {
  const { data } = pageContext;
  return (
    <Seo
      title={data.seo_title}
      description={data.seo_description}
      image={data.miniature[0].url}
    />
  );
};

export default Post;
