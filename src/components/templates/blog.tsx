import { Pagination } from '@/components/molecules/Pagination';
import { PostList } from '@/components/organisms/PostList';
import { Seo } from '@/components/Seo';
import { BlogPosts } from '@/interfaces';
import { BlogLayout } from '@/layouts/BlogLayout';
import { graphql } from 'gatsby';

const Blog = (props: BlogPosts) => {
  const { data, pageContext } = props;
  const posts = data.allStrapiPost.nodes;

  return (
    <BlogLayout>
      <PostList posts={posts} />
      <Pagination pageContext={pageContext} />
    </BlogLayout>
  );
};

export const Head = () => (
  <Seo
    title="Blog sobre programación"
    description="Web sobre desarrollo Web, programación Javascript, React, Nodejs"
  />
);
export default Blog;

export const query = graphql`
  query ($skip: Int!, $limit: Int!) {
    allStrapiPost(
      skip: $skip
      limit: $limit
      sort: { fields: createdAt, order: DESC }
    ) {
      nodes {
        id
        title
        url
        content
        createdAt
        miniature {
          url
        }
      }
    }
  }
`;
