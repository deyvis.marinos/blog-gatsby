import type { GatsbyConfig } from 'gatsby';

require(`dotenv`).config({
  path: `.env.${process.env.NODE_ENV}`,
});

const strapiConfig = {
  apiURL: process.env.STRAPI_API_URL,
  queryLimit: 1000, // Defaults to 100
  collectionTypes: [`post`],
};

const config: GatsbyConfig = {
  // Since `gatsby-plugin-typescript` is automatically included in Gatsby you
  // don't need to define it here (just if you need to change the options)
  siteMetadata: {
    title: `Blog Deyvis`,
    description: `Blog Deyvis para la comunidad`,
    author: `@deyvis17gy`,
  },
  plugins: [
    {
      resolve: `gatsby-source-strapi`,
      options: strapiConfig,
    },
    `gatsby-plugin-sass`,
    `gatsby-awesome-pagination`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `blog deyvis gatsby`,
        short_name: `blog deyvis`,
        start_url: `/`,
        background_color: `#663399`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/static/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
  ],
  jsxRuntime: `automatic`,
};

export default config;
