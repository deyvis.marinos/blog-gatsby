import { Description } from '@/components/atoms/Description';
import { Courses } from '@/components/molecules/Courses';
import { SocialMedia } from '@/components/molecules/SocialMedia';
import { Menu } from '@/components/organisms/Menu';
import { listCourses, listSocialMedia } from '@/components/organisms/Menu/data';
import { render, screen } from '@testing-library/react';

describe(`Menu`, () => {
  it(`renders a Menu component`, () => {
    render(<Menu />);

    expect(screen.getByText(/Blog Developer/)).toBeInTheDocument();
  });

  it(`renders a SocialMedia component`, () => {
    render(<SocialMedia listSocial={listSocialMedia} />);

    expect(screen.getByTestId(`social-media-1`)).toHaveAttribute(
      `href`,
      `https://www.youtube.com/channel/UC-lHJZR3Gqxm24_Vd_AJ5Yw`,
    );
  });

  it(`renders a Courses component`, () => {
    render(<Courses listCourses={listCourses} />);

    expect(screen.getByTestId(`course-1`)).toHaveAttribute(
      `href`,
      `https://www.udemy.com/course/react-y-electron/`,
    );
  });

  it(`renders a Description component`, () => {
    render(
      <Description
        text="Blog sobre desarrollo web, programación, tecnologías, diseño, desarrollo
        de videojuegos, etc."
      />,
    );

    expect(screen.getByText(/Blog/i)).toBeInTheDocument();
  });
});
