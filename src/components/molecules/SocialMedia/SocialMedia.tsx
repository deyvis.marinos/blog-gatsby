import { SocialMediaProps } from '@/interfaces';
import { map } from 'lodash';
import { Icon } from 'semantic-ui-react';
import './socialMedia.scss';

export const SocialMedia = ({ listSocial }: SocialMediaProps) => {
  return (
    <article className="m-socialMedia">
      {map(listSocial, (social) => (
        <a
          key={social.id}
          href={social.link}
          target="_blank"
          rel="noopener noreferrer"
          data-testid={`social-media-${social.id}`}
        >
          <Icon
            circular
            name={social.name}
            size="small"
            className={social.class}
          />
        </a>
      ))}
    </article>
  );
};
