import './description.scss';

type DescriptionProps = {
  text: string;
};
export const Description = ({ text }: DescriptionProps) => {
  return <p className="a-description">{text}</p>;
};
