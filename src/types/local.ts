import { SemanticICONS } from 'semantic-ui-react';

export type SocialMediaData = {
  id: number;
  name: SemanticICONS | undefined;
  link: string;
  class?: string;
};

export type CoursesData = {
  id: number;
  image: string;
  title: string;
  url: string;
};
