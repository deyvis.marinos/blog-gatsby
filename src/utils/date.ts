export const formatDate = (date: string) => {
  const dateObject = new Date(date);
  return dateObject.toLocaleDateString(`es`, {
    day: `numeric`,
    month: `long`,
    year: `numeric`,
  });
};
