import { render, screen } from '@testing-library/react';
import { Description } from './Description';

describe(`Description`, () => {
  it(`renders a Description component`, () => {
    render(<Description text="Test Description" />);

    expect(screen.getByText(`Test Description`)).toBeInTheDocument();
  });
});
