import { Description } from '@/components/atoms/Description';
import { Link } from 'gatsby';
import { Courses } from '../../molecules/Courses';
import { SocialMedia } from '../../molecules/SocialMedia';
import { listCourses, listSocialMedia } from './data';
import './menu.scss';

export const Menu = () => {
  return (
    <div className="menu">
      <Link to="/">
        <h2>Blog Developer</h2>
      </Link>
      <Description
        text="Blog sobre desarrollo web, programación, tecnologías, diseño, desarrollo
        de videojuegos, etc."
      />
      <SocialMedia listSocial={listSocialMedia} />
      <Courses listCourses={listCourses} />
    </div>
  );
};
