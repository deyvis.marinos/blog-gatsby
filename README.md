# Blog Gatsby

course blog using strapi

## Demo

https://blog-gatsby-dyv.netlify.app/

## Screenshots

![App Screenshot](docs/images/home.png)

## Tech Stack

**Client:** Html, Css, BEM, Sass, Gatsby

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/deyvis.marinos/blog-gatsby.git
```

Go to the project directory

```bash
  cd blog-gatsby
```

```bash
 yarn install
```

```bash
  yarn start
```
