import { PaginationProps } from '@/interfaces';
import { Link } from 'gatsby';
import PropType from 'prop-types';
import './pagination.scss';
export const Pagination = ({ pageContext }: PaginationProps) => {
  const { previousPagePath, nextPagePath } = pageContext;
  return (
    <footer className="m-pagination">
      {previousPagePath && (
        <Link to={previousPagePath} rel="prev" className="m-pagination__button">
          Previous
        </Link>
      )}
      {nextPagePath && (
        <Link to={nextPagePath} rel="next" className="m-pagination__button">
          Next
        </Link>
      )}
    </footer>
  );
};

Pagination.prototype = {
  pageContext: PropType.object.isRequired,
};
