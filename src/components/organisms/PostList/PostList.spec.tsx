import { render } from '@testing-library/react';
import { mockPosts } from '../Menu/data';
import { PostList } from './PostList';

describe(`render PostList Components`, () => {
  it(`should render a PostList component`, () => {
    const { getByText } = render(<PostList posts={mockPosts} />);
    expect(getByText(/Aprendiendo Gatsby/)).toBeInTheDocument();
  });
});
