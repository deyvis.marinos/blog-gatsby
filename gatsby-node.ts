import type { GatsbyNode } from 'gatsby';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';

import { paginate } from 'gatsby-awesome-pagination';

const path = require(`path`);

export const onCreateWebpackConfig: GatsbyNode['onCreateWebpackConfig'] = ({
  actions,
}) => {
  actions.setWebpackConfig({
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
    },
  });
};

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;

  const posts = await graphql(`
    query {
      allStrapiPost(sort: { fields: createdAt, order: DESC }) {
        nodes {
          id
          title
          url
          content
          createdAt
          miniature {
            url
          }
          seo_title
          seo_description
        }
      }
    }
  `);

  paginate({
    createPage, // The Gatsby `createPage` function
    items: posts.data.allStrapiPost.nodes, // An array of objects
    itemsPerPage: 5, // How many items you want per page
    pathPrefix: `/`, // Creates pages like `/blog`, `/blog/2`, etc
    component: path.resolve(`src/components/templates/blog.tsx`), // Just like `createPage()`
  });

  posts.data.allStrapiPost.nodes.forEach((post) => {
    createPage({
      path: `post/` + post.url,
      component: path.resolve(`src/components/templates/post/post.tsx`),
      context: {
        data: post,
      },
    });
  });
};
