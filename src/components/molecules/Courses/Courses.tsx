import { CoursesProps } from '@/interfaces';
import { map } from 'lodash';
import { Image } from 'semantic-ui-react';
import './courses.scss';
export const Courses = ({ listCourses }: CoursesProps) => {
  return (
    <article className="m-courses">
      {map(listCourses, (course) => (
        <a
          key={course.id}
          href={course.url}
          target="_blank"
          rel="noopener noreferrer"
          className="courses__item"
          data-testid={`course-${course.id}`}
        >
          <Image src={course.image} alt={course.title} />
        </a>
      ))}
    </article>
  );
};
