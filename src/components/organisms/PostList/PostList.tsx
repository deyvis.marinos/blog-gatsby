import { PostListProps } from '@/interfaces';
import { formatDate } from '@/utils/date';
import { Link } from 'gatsby';
import { map } from 'lodash';
import { Card, Icon, Image } from 'semantic-ui-react';
import './postList.scss';

const urlBack = process.env.STRAPI_API_URL;

export const PostList = ({ posts }: PostListProps) => {
  return (
    <section className="post-list">
      {map(posts, (post) => (
        <article key={post.id}>
          <Card className="post-list__card">
            <Image
              src={urlBack + post.miniature[0]?.url}
              alt={post.title}
              className="post-list__image"
            />
            <Card.Content>
              <Link to={`/post/${post.url}`}>
                <Card.Header>{post.title}</Card.Header>
              </Link>
            </Card.Content>
            <Card.Content extra>
              <Card.Meta>
                <Icon name="calendar alternate outline" />
                {formatDate(post.createdAt)}
              </Card.Meta>
            </Card.Content>
          </Card>
        </article>
      ))}
    </section>
  );
};
