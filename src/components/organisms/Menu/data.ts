import electronReact from '../../../static/cursos/electron-react.jpg';
import instaClone from '../../../static/cursos/instaclone.jpg';
import reactNativeExpo from '../../../static/cursos/react-native-expo.png';
import reactNativeCli from '../../../static/cursos/react-native-cli.jpg';
import { CoursesData, SocialMediaData } from '@/types/local';
import { BodyResponse } from '@/interfaces';

export const listSocialMedia: SocialMediaData[] = [
  {
    id: 1,
    name: `youtube`,
    link: `https://www.youtube.com/channel/UC-lHJZR3Gqxm24_Vd_AJ5Yw`,
    class: `youtube`,
  },
  {
    id: 2,
    name: `twitter`,
    link: `https://twitter.com/DeyvisMarinos`,
    class: `twitter`,
  },
  {
    id: 3,
    name: `linkedin`,
    link: `https://www.linkedin.com/in/deyvisdev/`,
    class: `linkedin`,
  },
];

export const listCourses: CoursesData[] = [
  {
    id: 1,
    image: electronReact,
    title: `Electron y React`,
    url: `https://www.udemy.com/course/react-y-electron/`,
  },
  {
    id: 2,
    image: instaClone,
    title: `InstaClone`,
    url: `https://www.udemy.com/course/instaclone-desarrollo-web-con-react-js/`,
  },
  {
    id: 3,
    image: reactNativeExpo,
    title: `React Native y Expo`,
    url: `https://www.udemy.com/course/react-native-y-expo/`,
  },
  {
    id: 4,
    image: reactNativeCli,
    title: `React Native CLI`,
    url: `https://www.udemy.com/course/react-native-cli/`,
  },
];

export const mockPosts: BodyResponse[] = [
  {
    id: `Post_62f5e426e8cad86f605b04f5`,
    title: `Aprende a crear un clon de trello`,
    url: `aprende-a-crear-un-clon-de-trello`,
    content: `Clon de trello con vuejs`,
    createdAt: `2022-08-12T05:24:54.905Z`,
    miniature: [
      {
        url: `/uploads/trello_661a8c2023.png`,
      },
    ],
    seo_title: `Test`,
    seo_description: `Test`,
  },
  {
    id: `Post_62f5e3c8e8cad86f605b04f2`,
    title: `Curso de Angular`,
    url: `curso-de-angular`,
    content: `<figure class="image"><img src=""></figure><figure class="image"><img src="https://d7lju56vlbdri.cloudfront.net/var/ezwebin_site/storage/images/_aliases/img_1col/noticias/solar-orbiter-toma-imagenes-del-sol-como-nunca-antes/9437612-1-esl-MX/Solar-Orbiter-toma-imagenes-del-Sol-como-nunca-antes.jpg"></figure><p>![kanban.png](http://localhost:1337/uploads/kanban_a1ca8693bc.png)</p><p>[text](link)Aprende a crear tu propio KANBAN \`\`\`code block\`\`\` [Canal](https://youtu.be/43VXsjzMLcw)</p><p>&nbsp;</p><figure class="image"><img src=""></figure><figure class="media"><oembed url="https://youtu.be/43VXsjzMLcw"></oembed></figure><p>&nbsp;</p>`,
    createdAt: `2022-08-12T05:23:20.206Z`,
    miniature: [
      {
        url: `/uploads/kanban_a1ca8693bc.png`,
      },
    ],
    seo_title: `Test`,
    seo_description: `Test`,
  },
  {
    id: `Post_62f2d0465322e776802f3cdb`,
    title: `Aprendiendo Gatsby`,
    url: `aprendiendo-gatsby`,
    content: `<p>Gatsby <a href="https://youtu.be/oE9GGrdrH1U">https://youtu.be/oE9GGrdrH1U</a>&nbsp;</p><figure class="media"><oembed url="https://youtu.be/oE9GGrdrH1U"></oembed></figure>`,
    createdAt: `2022-08-09T21:23:18.562Z`,
    miniature: [
      {
        url: `/uploads/gatsby_icon_7f7c00078d.png`,
      },
    ],
    seo_title: `Test`,
    seo_description: `Test`,
  },
];
